package com.calculator.contractsalary.web;

import com.calculator.contractsalary.ContractSalaryApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/*Integration tests in this case are valid only during the same day or until next update
of given exchange rate on http://www.nbp.pl website*/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ContractSalaryApplication.class)
@AutoConfigureMockMvc
public class CalculatorControllerTest {

    private static final String URL = "/calculator/{currency-code}/{daily-wage}";

    @Autowired
    MockMvc mockMvc;

    @Test
    public void shouldReturnCorrectResultForPL() throws Exception{
        mockMvc.perform(get(URL, "pln", new BigDecimal(130)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(1116.60));
    }

    @Test
    public void shouldReturnCorrectResultForDE() throws Exception{
        mockMvc.perform(get(URL, "EUR", 125))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(5987.94));
    }

    @Test
    public void shouldReturnCorrectResultForUK() throws Exception{
        mockMvc.perform(get(URL, "GBP", 70.50))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(2727.82));
    }
}