package com.calculator.contractsalary.repository;


import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Repository
public class ConstantDataRepository {

    private static Map<String, ConstantData> constantDataMap;

    static {
        constantDataMap = new HashMap<String, ConstantData>() {
            {
                put("GBP", new ConstantData(new BigDecimal(0.25), new BigDecimal(600)));
                put("EUR", new ConstantData(new BigDecimal(0.20), new BigDecimal(800)));
                put("PLN", new ConstantData(new BigDecimal(0.19), new BigDecimal(1200)));
            }
        };
    }

    public static ConstantData getDataByCountryCode(String countryCode) {
        return constantDataMap.get(countryCode);
    }
}
