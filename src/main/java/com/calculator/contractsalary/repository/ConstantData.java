package com.calculator.contractsalary.repository;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class ConstantData {

    private BigDecimal incomeTax;
    private BigDecimal fixedCosts;
}
