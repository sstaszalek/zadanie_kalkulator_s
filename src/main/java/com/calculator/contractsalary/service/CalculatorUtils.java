package com.calculator.contractsalary.service;

import com.calculator.contractsalary.repository.ConstantData;
import com.calculator.contractsalary.repository.ConstantDataRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class CalculatorUtils {

    private static final int WORKING_DAYS = 22;

    public static BigDecimal CalculateSalary(BigDecimal dailyWage, String currencyCode, BigDecimal exchangeRate) {

        ConstantData data = ConstantDataRepository.getDataByCountryCode(currencyCode.toUpperCase());

        BigDecimal fixedCost = data.getFixedCosts();
        BigDecimal incomeTax = data.getIncomeTax();

        return dailyWage
                .multiply(new BigDecimal(WORKING_DAYS))
                .multiply(BigDecimal.ONE.subtract(incomeTax))
                .subtract(fixedCost)
                .multiply(exchangeRate)
                .setScale(2, RoundingMode.HALF_UP);
    }
}
