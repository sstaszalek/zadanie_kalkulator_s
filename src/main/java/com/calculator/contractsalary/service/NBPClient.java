package com.calculator.contractsalary.service;

import com.calculator.contractsalary.domain.ExchangeRates;
import com.calculator.contractsalary.domain.Rate;
import com.calculator.contractsalary.errorHandling.NbpException;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;

@Service
public class NBPClient {

    private static final String NBP_URL = "http://api.nbp.pl/api/exchangerates/rates/a/{code}/";

    private final RestTemplate restTemplate;


    public NBPClient(RestTemplateBuilder builder) {
        restTemplate = builder.build();
    }

    private ResponseEntity<ExchangeRates> fetchData(String currencyCode) {
        try {
            return this.restTemplate.getForEntity(NBP_URL, ExchangeRates.class, currencyCode);
        } catch (HttpClientErrorException ex) {
            throw new NbpException(ex.getMessage());
        }
    }

    public BigDecimal getCalculation(BigDecimal dailyWage, String currencyCode) {
        if (currencyCode.toUpperCase().equals("PLN")) {
            return CalculatorUtils.CalculateSalary(dailyWage, currencyCode, BigDecimal.ONE);
        } else {
            try {
                List<Rate> response = fetchData(currencyCode).getBody().getRates();
                BigDecimal actualExchangeRate = response.get(0).getMid();
                return CalculatorUtils.CalculateSalary(dailyWage, currencyCode, actualExchangeRate);
            } catch (HttpClientErrorException ex) {
                throw new NbpException(ex.getMessage());
            }
        }
    }
}
