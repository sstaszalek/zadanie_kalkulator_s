package com.calculator.contractsalary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContractSalaryApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContractSalaryApplication.class, args);
	}
}
