package com.calculator.contractsalary.errorHandling;

public class NbpException extends RuntimeException {

    public NbpException(String message) {
        super(message);
    }
}
