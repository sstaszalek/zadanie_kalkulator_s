package com.calculator.contractsalary.web;

import com.calculator.contractsalary.service.NBPClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class CalculatorController {

    private final NBPClient nbpClient;

    public CalculatorController(NBPClient nbpClient) {
        this.nbpClient = nbpClient;
    }

    @RequestMapping(path = "/calculator/{currency-code}/{daily-wage}")
    public BigDecimal calculateSalaryByCountryCode(@PathVariable("currency-code") String currencyCode,
                                                   @PathVariable("daily-wage") BigDecimal dailyWage) {
        return this.nbpClient.getCalculation(dailyWage, currencyCode);
    }
}
