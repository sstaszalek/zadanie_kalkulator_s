(function() {
  
  var app = angular.module("salaryComputer", []);
  
  var CalculatorController = function($scope, $http) {

    $scope.calculateSalaryByCountryCode = function() {
        var currencyCode = $scope.currencyCode;
        var dailyWage = $scope.dailyWage;
        var serviceUrl = "/calculator/"+ currencyCode +"/" + dailyWage

        $http.get(serviceUrl).then(onSalaryComputeComplete, onError);
    }

    var onSalaryComputeComplete = function(response) {
      $scope.salaryResponse = response.data;
      $scope.showResult = true;
    };

    var onError = function(reason) {
      $scope.error = "Couldn't fetch NBP currency data";
      $scope.showError = true;
    };


  };
  app.controller("CalculatorController", CalculatorController);
  
}());