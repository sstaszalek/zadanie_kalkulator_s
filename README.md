# Contract Salary Calculator
Simple Web Service made in Spring framework that allows to calculate monthly net salary in Polish currency (PLN). Calculation might be also done
when working in Great Britain (GBP) and Germany (EUR). The other countries can be implemented.

The application gets current currency rates from the official National Polish Bank website: http://www.nbp.pl 

User only gives daily wages and chooses the country. Result is always given in PLN.
It assumes different fixed costs and taxes in each country.

This project can be runned without user interface or combined with AngularJS frontend (css styles need to be
still improved).


## How to use this program


Once application is up and running, please open your web browser and navigate to address:

> Using URL address.
In order to make calculation, please pass values of currency code and daily wages in URL for example:

```
http://localhost:8080/calculator/EUR/160.00
```

> Using html template with Angular. Go to the following address:
```
http://localhost:8080
```
The easiest way to write this app from sratch is by creating Maven project
in Spring Initializer: http://start.spring.io

##### Required dependencies:
- Spring Web
- Lombok

